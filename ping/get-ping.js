import { check, sleep } from "k6";
import { Counter, Rate } from "k6/metrics";
import http from "k6/http";

let errorCounter = new Counter("errors_getping");

var data = require('../data.js');

const request = () => {

    var params = {
        timeout: 1200000,
        headers: {
            'Content-Type': 'application/json',
        }
    };

    var res = http.get(data.baseUrl, params);

    if (res.status !== 200) {
        errorCounter.add(1)
        console.log(res.status+ " | GET Ping ")
    }

    return res;
}

export default function() {
    check(request(), {
        "200 GET Ping": r => r.status === 200
    });
}
