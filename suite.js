// GET
import GetPing from "./ping/get-ping.js";

export let options = {
  setupTimeout: '120s',
  teardownTimeout: '120s',
  discardResponseBodies: true,
  scenarios: {
    constant_request_rate : {
      executor: 'per-vu-iterations',
      vus: 100,
      iterations: 1,
      maxDuration: '1m',
    }
  }
};

export default() => {
    GetPing();
}
